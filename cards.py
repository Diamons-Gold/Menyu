from PIL import Image, ImageFont, ImageDraw
import random as r
import os

class Card:
    def __init__(self, dict_cards, dict_cards_effects):
        self.path = os.path.dirname(os.path.abspath(__file__))

        if type(dict_cards) == list:
            self.card_data = self.select_cards(dict_cards)

        else:
            self.card_data = dict_cards

        self.background_data_img = Image.open(f'{self.path}/assets/texture/cards/cardBackground.png')
        self.framework_data_img = Image.open(f'{self.path}/assets/texture/cards/cardFramework.png')
        self.bar_data_img = Image.open(f'{self.path}/assets/texture/cards/cardBar.png')

        self.weapon_data_img = Image.open(f'{self.path}/assets/texture/weapon/weapon_48x48.png')

        self.font_data_64 = ImageFont.truetype(f'{self.path}/assets/fonts/m5x7.ttf', 64)
        self.font_data_48 = ImageFont.truetype(f'{self.path}/assets/fonts/m5x7.ttf', 48)
        self.font_data_32 = ImageFont.truetype(f'{self.path}/assets/fonts/m5x7.ttf', 32)

        if self.card_data['effect'] != "":
            self.effect = dict_cards_effects[int(self.card_data['effect'])]
            self.desc = self.card_data['description']

        else:
            self.effect = None
            
        self.name = self.card_data['name']
        self.type = self.card_data['type']
        self.pts_effect = int(self.card_data['pts_effect'])
        self.mana_price = int(self.card_data['mana_price'])
        self.rarity = int(self.card_data['rarity'])
        self.description = self.card_data['description']
        self.img = self.card_data['img']


    def select_cards(self, dict_cards):
        '''
        Precondition : get a list with dict of cards.
        Postcondition : return one dict of card.
        '''
        card = dict_cards[r.randint(0, len(dict_cards)-1)]
        
        if r.randint(0,100) <= int(card['rarity']):
            return card
        
        return self.select_cards(dict_cards)


    def price_and_mana(self):
        '''
        Postcondition : return the point of effect dans the price of the card.
        '''
        return self.pts_effect, self.mana_price


    def get_name(self):
        '''
        Postcondition : give the name of the card withn't spaces.
        '''
        return self.name.replace(" ", "")


    def use_card(self, user, target, effects):
        '''
        Précondition : take the user class, the target class and the class effects.
        Postcondition : use the cards.
        '''
        if self.effect != None:
            effects.activation("use")

        if self.type == "damage":
            target.life += self.pts_effect + user.bonus_damage

        elif self.type == "heal":
            user.life += self.pts_effect


    def card_background(self):
        '''
        Postcondition : return a number betwenn 0 and 3, according to the type of the card.
        '''
        if self.type == "damage":
            return 0

        elif self.type == "curse":
            return 1

        elif self.type == "heal":
            return 2

        return 3


    def img_rarity(self):
        '''
        Postcondition : return a number betwenn 0 and 2, according to the rarity of the card.
        '''
        if self.rarity >= 50:
            return 0

        elif self.rarity > 10:
            return 1

        return 2


    def get_surface(self):
        '''
        Postcondition : return a number a surface (x, y, width, height), according to the weapon of the card.
        '''
        if self.type == 'damage':
            return 192, 1008, 240, 1056

        elif self.type == 'curse':
            return 624, 864, 672, 912

        elif self.type == 'heal':
            return 336, 720, 384, 768

        elif self.type == 'boost':
            return 0, 144, 48, 192


    def save_img(self):
        '''
        Postcondition : save the image with the name in .png on the folder game_data.
        '''
        name = self.get_name()
        self.img.save(f"assets/game_data/{name}.png")


    def del_img(self):
        '''
        Precondition : the function save_img as been use.
        Postcondition : delete the image on the folder game_data.
        '''
        name = self.get_name()
        os.remove(f"assets/game_data/{name}.png")

    
    def ligne_text(self, desc):
        '''
        Preconditon : take a str of the description for the card.
        Postcondition : return a list of the description cut in few parts.
        '''
        i = 21
        if len(desc) > i:
            if desc[i] == ".":
                return [desc]
            else:
                while desc[i] != " ":
                    i -= 1
                    
                if desc[i] == " ":
                    rest = desc[i + 1:]
                else:
                    rest = desc[i:]

                return [desc[:i]] + self.ligne_text(rest)

        return [desc]


    def start_desc(self, line):
        '''
        Precondition : get nombers of line for the description.
        Postcondition : return the y who the description need to start depending to the nomber of line.
        '''
        if line == 1:
            return 415

        elif line == 2:
            return 400

        elif line == 3:
            return 385


    def text_center(self, text_y):
        '''
        Precondition : take the y of the text.
        Postcondition : return y or y +10 to center the text.
        '''
        len_size = len(self.name)*5
        if len_size > 65:
            return text_y + 10
        return text_y


    def text_size(self):
        '''
        Postcondition : return font size according to the size of the text.
        '''
        size = len(self.name)*5
        if size > 65:
            return self.font_data_48

        return self.font_data_64


    def make_card(self):
        i = self.card_background()
        background = self.background_data_img.crop((i*110, 0, (i+1)*100 + i*10, 128)).convert("RGBA")
        background = background.resize((400,512), resample=Image.BOX)

        i = self.img_rarity()
        framework = self.framework_data_img.crop((i*96, 0, (i+1)*86 + i*10, 72)).convert("RGBA")
        framework = framework.resize((240,240), resample=Image.BOX)

        skill = Image.open(f'{self.path}/assets/texture/skills_texture/skill_icons{self.img}.png')
        skill = skill.crop((4, 4, 91 ,91))
        skill = skill.resize((218, 218), resample=Image.BOX)
        
        bar = self.bar_data_img.crop((i*106, 0, (i+1)*96 + i*10, 29)).convert("RGBA")
        bar = bar.resize((344, 104), resample=Image.BOX)

        small_bar = self.bar_data_img.crop((i*106, 39, (i+1)*96 + i*10, 57)).convert("RGBA")
        small_bar = small_bar.resize((344, 64), resample=Image.BOX)

        mana_potion = self.weapon_data_img.crop((288, 720, 336, 768)).convert("RGBA")
        mana_potion = mana_potion.resize((96, 96), resample=Image.BOX)

        surface = self.get_surface()
        effect = self.weapon_data_img.crop(surface).convert("RGBA")
        effect = effect.resize((80, 80), resample=Image.BOX)

        background.paste(skill, (91, 52), skill)
        background.paste(framework, (80, 40), framework)
        background.paste(bar, (28, 380), bar)
        background.paste(small_bar, (28, 300), small_bar)
        background.paste(mana_potion, (288, 28), mana_potion)
        background.paste(effect, (40, 392), effect)

        background_draw = ImageDraw.Draw(background)
        background_draw.text((40, self.text_center(300)), f'- {self.name}', font=self.text_size())
        background_draw.text((328, 60), f'{self.mana_price}', font=self.font_data_64)

        if self.effect == None:
            background_draw.text((140, 400), f'{self.pts_effect}', font=self.font_data_64)

        else:
            dict_text = self.ligne_text(self.desc)
            for i in range(len(dict_text)):
                background_draw.text((130, self.start_desc(len(dict_text)) + i * 30), f'{dict_text[i]}', font=self.font_data_32)

        self.img = background
        self.save_img()
