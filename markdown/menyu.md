<link rel="stylesheet" type="text/css" media="all" href="style.css"/>

# Menyu

Menyu is card game made by 2 person in 2021.

## Card example :

The four following types is Damage, Curse, Heal and Boost.

![alt text](img/Redbeam.png "Red beam card")
![alt text](img/Poison.png "Poison card")
![alt text](img/Life+.png "Life + card")
![alt text](img/Shield.png "Shield card")

## Credits :

Thanks for the assets

[Cainos](https://cainos.itch.io) for the tilesets of the map. <br/>
[9e0](https://9e0.itch.io) for the Witch assets. <br/>
[Cafedraw](https://cafedraw.itch.io) for the cards assets. <br/>
[Managore](https://managore.itch.io) for the font. <br/>
[Kyrise](https://kyrise.itch.io) for the weapon assets. <br/>
[Quintino pixel](https://quintino-pixels.itch.io) for the skills assets.
