class Cards_effects:
    def __init__(self):
        self.memory = []


    def add_effect(self, effect, user, target):
        '''
        Précondition : effect the dict effect, user "player" or "ennemy" and target "player" or "ennemy".
        '''
        effect["value"] = int(effect["value"])
        if effect["target"] == "self":
            effect["target"] = user
        if effect["target"] == "ennemy":
            effect["target"] = target

        self.memory.append(effect)


    def del_effect(self, index):
        for i in index:
            self.memory.pop([i])


    def activation(self, event, player, ennemy):
        end_effect = []

        for i in range(len(self.memory)):
            if self.memory[i]["activation"] == event:
                self.use_effect(i, player, ennemy)
                self.memory[i]["value"] -= 1
                if self.memory[i]["value"] > 0:
                    end_effect.append(i)

        self.del_effect(end_effect)


    def use_effect(self, index, player, ennemy):
        effect = self.memory[index]["effect"]

        if effect["target"] == None:
            return

        if effect["target"] == "player":
            player.get_attribute(effect)
        elif effect["target"] == "ennemy":
            ennemy.get_attribute(effect)
