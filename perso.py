from cards import Card
from data import Data
import pygame

# pbm: dans print_hand() -> bien remettre 2nd dico avec cards_effects.csv
class Player_Game:
    
    def __init__(self):
        self.pv = 1000
        self.mana = 5
        self.shield = 0
        self.bonus_damage = 0
        
        self.memory = []
        
    def __repr__(self):
        return 'pv:'+self.vie+', mana:'+self.mana
    
    def choose_card(self):
        pass
    
    def print_selected_card(self, screen, width, height, name_card):
        """
        width -> width of the screen
        height -> height of the screen
        name_card -> num of card selected
    
        => print the card on the screen
        """
    
        one_card = pygame.image.load('assets/game_data/image'+name_card+'.png').convert_alpha()
        one_card = pygame.transform.scale(one_card, (width//4, height//3))
        screen.blit(one_card, (width//2 - width//8 , height//2 - height//6))
    
    def print_card(self,screen, width, height, card, name_card, x=0):
        """
        x -> décalage entre les cartes quand faut en afficher plusieurs
        width -> width of the screen
        height -> height of the screen
        card -> instance of Card()
        name_card -> num of card created
    
        => print the card on the screen
        """
        card.make_card()
        one_card = pygame.image.load('assets/game_data/image'+name_card+'.png').convert_alpha()
        one_card = pygame.transform.scale(one_card, (width//8, height//7))
        screen.blit(one_card, (width//15 + x  , 8.5*height//10))

    def print_hand(self, screen, width, height):
        """
        screen -> the screen
        width -> width of the screen
        height -> height of the screen
    
        => print the card on the screen
        """
        marg = 0
        temp_cards = [] #pour retenir les instances créées
        for hand in range(5):
            card = Card(Data().import_cards_csv('/assets/cards.csv'), Data().import_cards_csv('/assets/cards.csv'))
            temp_cards.append(card)
            self.print_card(screen, width, height, card , str(hand), marg)
            marg += width//8 + width//15
        
        return temp_cards 
            
            
            
            
            
            
            
            
#independant tour monstre ou joueur ?          
def get_attribute(self, attribut):
    return self.attribut