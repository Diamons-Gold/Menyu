import pygame as p

class Menu:
    def __init__(self, size):
        self.select = 0
        self.font = p.font.Font(f'assets/fonts/m5x7.ttf', 64)

        self.text = []
        self.text.append(self.font.render("New Game", True, (255, 255, 255)))
        self.text.append(self.font.render("Credits", True, (255, 255, 255)))
        self.text.append(self.font.render("Exit", True, (255, 255, 255)))

        self.text_select = []
        self.text_select.append(self.font.render("New Game", True, (0, 0, 128)))
        self.text_select.append(self.font.render("Credits", True, (0, 0, 128)))
        self.text_select.append(self.font.render("Exit", True, (0, 0, 128)))

        self.bg = p.Surface(size)
        self.bg.set_alpha(128)
        self.bg.fill((0, 0, 0)) 

        self.credits = [
            "https://cainos.itch.io -> Tilesets for map",
            "https://9e0.itch.io -> Witch",
            "https://cafedraw.itch.io -> Cards Assets",
            "https://managore.itch.io -> Font",
            "https://kyrise.itch.io -> Weapon",
            "https://quintino-pixels.itch.io -> Skills"
        ]

        self.font = p.font.Font(f'assets/fonts/m5x7.ttf', 32)

        for i in range(len(self.credits)):
            self.credits[i] = self.font.render(self.credits[i], True, (255, 255, 255))


    def update(self, screen, index):
        screen.blit(self.bg, (0, 0))
        for i in range(len(self.text)):
            if i != index:
                screen.blit(self.text[i], (375, 425 + 50*i))
            else:
                screen.blit(self.text_select[i], (375, 425 + 50*i))


    def credits_update(self, screen):
        screen.blit(self.bg, (0, 0))
        for i in range(len(self.credits)):
            screen.blit(self.credits[i], (50, 150 + 50*i))
